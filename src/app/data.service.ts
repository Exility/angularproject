import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IBook } from './book';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  private formData = new BehaviorSubject<any>({});
  currentData = this.formData.asObservable();
  constructor(private http: HttpClient) { }

  updateData(data: any) {
    this.formData.next(data);
  }
  fetchData(): Observable<any> {
    const headers = new HttpHeaders().set('Access-Control-Allow-Origin','*');
    return this.http.get<IBook>('https://potterapi-fedeperin.vercel.app/en/books');
  }
}