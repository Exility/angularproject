import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DataService } from '../data.service';  
@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrl: './edit-form.component.css'
})
export class EditFormComponent {
  @Input() book = new Book('','',0);
  @Input() bookIndex=0;
  @Output() valueChanged = new EventEmitter<boolean>();
  formData: any = {};

  constructor(private dataService: DataService) { }
  updateParent(value: boolean) {
    this.valueChanged.emit(value);
  }
  onSubmit(id:number, title:string,author:string,pages:number) {
    const formData = {id,title,author,pages};
    this.dataService.updateData(formData);
  }
}
class Book {
  constructor(public title: string, public author: string, public pages: number) { }
}
