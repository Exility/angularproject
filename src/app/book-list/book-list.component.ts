import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataService } from '../data.service';
import { tap, catchError } from 'rxjs/operators';
import { IBook } from '../book';
const COLUMNS_SCHEMA = [
  {
      key: "title",
      type: "text",
      label: "Full Title"
  },
  {
      key: "author",
      type: "text",
      label: "Author"
  },
  { 
      key: "pages",
      type: "number",
      label: "Pages"
  },
  {
    key: "isEdit",
    type: "isEdit",
    label: ""
}
]
@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrl: './book-list.component.css'
})
export class BookListComponent implements OnInit {
  formData: any;

  constructor(private dataService: DataService, private cdr: ChangeDetectorRef) { }
  fetchedData: any;
  showDetails=false;
  onChildValueChange(newValue: boolean) {
    this.showDetails = newValue;
  }
  displayedColumns: string[] = COLUMNS_SCHEMA.map((col) => col.key);
  columnsSchema: any = COLUMNS_SCHEMA;
  dataSource: any;
  selectedBook = 0;
  ngOnInit(): void{
    this.fetchData();
    this.dataService.currentData.subscribe(data => {
      this.formData = data;
      let bookToAdd = new Book(data.title,data.author,data.pages);
      if(this.fetchedData){
        this.fetchedData[this.selectedBook] = bookToAdd;
      }
      this.cdr.detectChanges();
    });
  }
  fetchData(): void {
    this.dataService.fetchData()
      .pipe(
        tap((data) => {
          console.log(data);
          this.fetchedData =[];
          for(let i=0;i<data.length;i++){
            let iBook = data[i];
            this.fetchedData[i] = new Book(iBook.title,"J.K.Rowling",iBook.pages);
          }
          this.dataSource = this.fetchedData;
          console.log('Dane otrzymane:', this.fetchedData);
        }),
        catchError((error) => {
          console.error('Błąd podczas pobierania danych:', error);
          throw error; // Rzucenie błędu, aby przekazać go dalej
        })
      )
      .subscribe();
  }
}
class Book {
  constructor(public title: string, public author: string, public pages: number) { }
}