import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoComponent } from './info/info.component';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [
    InfoComponent  ],
  imports: [
    CommonModule,
    NgbNavModule,
    HttpClientModule
  ],
  exports:[
    InfoComponent
  ]
})
export class MainModule { }
